//global array holding the timings
var timings = [];

//global counter denoting sample number
var counter = 0;

//set up event listeners for http requests.
/*Upon further testing it seems that when metamask is absent from the browser and 
local node is used, all network requests are being done within the page's 'enclosed' 
environment. Thus the IR extension is able to catch such requests. 

However the same cannot be said when metamask is installed. In such a scenario all 
JSON RPC requests are being done in the metamask 'enclosed' environment, as can be 
observed from the network view of both the page and metamask extension in developer's mode.

Now according to these two threads, the ability to observe network requests in other 
extension's environment was deemed as a security bug and removed. Thus such requests 
cannot be observed when a browser wallet is used unless in debugger mode.

https://stackoverflow.com/questions/37926493/is-it-possible-to-catch-requests-from-another-extension
https://stackoverflow.com/questions/46222289/how-to-intercept-other-extensions-requests-using-chrome-extension-api*/
chrome.webRequest.onBeforeRequest.addListener(
	()=>{
		let msg = {
			request_type: 'onBeforeRequest'
		}

		sendMsgContentScript(msg);
	},
	{urls: ["*://localhost:*/"]},
	["blocking"]
);

chrome.webRequest.onResponseStarted.addListener(
	()=>{
		let msg = {
			request_type: 'onResponseStarted'
		}

		sendMsgContentScript(msg);
	},
	{urls: ["*://localhost:*/"]}
);

chrome.webRequest.onCompleted.addListener(
	()=>{
		let msg = {
			request_type: 'onCompleted'
		}

		sendMsgContentScript(msg);
	},
	{urls: ["*://localhost:*/"]}
);

//sends message to content script running in the active tab
function sendMsgContentScript(msg){
	//send message to content script running in current active page
	//i.e. DApp 
	chrome.tabs.query({ active:true, currentWindow:true} , function(tab){
    	chrome.tabs.sendMessage( tab[0].id, msg);

	});
}


//listens for evidence being sent from content script
chrome.runtime.onMessage.addListener(getMessage);

//read Message recieved from content script and assign it appropriately
function getMessage(msg){
	switch(msg.type){
		case 'timings':
			let results = parseTimings(msg.timings);
			sendTimings(JSON.stringify(results));
			break;
		case 'evidence':
			getEvidence(msg.evidence);
			break;
		default:
			console.log('unkown message');
			break;
	}
}

//gets evidence from content script and packs it into a package.
//finally package is sent to server
function getEvidence(msg){
	let t0 = performance.now();
	let package = {
		'name': parseFileName(msg.TimeStamp),
		'evidence': msg 
	}
	let t1 = performance.now();
	let getEvidenceTiming = t1-t0;

	t0 = performance.now();
	sendEvidence(JSON.stringify(package));
	t1 = performance.now();
	let sendEvidenceTiming = t1-t0;

	let record = {
        'packEvidence': getEvidenceTiming,
        'sendEvidence': sendEvidenceTiming,
    };
    timings.push({[counter] : record});
    counter+=1;
}

//parses filename based on timestamp
function parseFileName(ts){
	return ts.Day+'_'+ts.Date.toString()+'_'+ts.Month+'_'+ts.Year.toString()+'_'+ts.Time;
}

//sends collected evidence to server so that server stores it.
//this is done by generating a Post request.
function sendEvidence(evidencePackage){
	console.log('sending evidence to server');
	var xhr = new XMLHttpRequest();
	xhr.open("POST", 'http:localhost:3000/saveEvidence', true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	//since xhr.open is set to true, send is method sends data asynchronously
	//thus returns as soon as the request is sent. (Result by server is handled by
	//events and is not waited upon)
	xhr.send(evidencePackage);
}

//parses timings recieved and combines them with the ones collected in bg script
//finally it parese the whole array into a single object
function parseTimings(timings_sent){
	let recievedTimings = JSON.parse(timings_sent);
	let mergedTimings = [];
	recievedTimings.forEach((itm,i)=>{
		mergedTimings.push(Object.assign({}, itm[i], timings[i][i]));
	});

	//convert array of objects into a single object
	let timings_obj = {};
	for(var i = 0; i < mergedTimings.length; i++){
		timings_obj[i] = mergedTimings[i];
	}

	return timings_obj;
}

//sends timings object to server
function sendTimings(timing_obj){
	console.log('sending timings to server');
	var xhr = new XMLHttpRequest();
	xhr.open("POST", 'http:localhost:3000/saveTimings', true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	//since xhr.open is set to true, send is method sends data asynchronously
	//thus returns as soon as the request is sent. (Result by server is handled by
	//events and is not waited upon)
	xhr.send(timing_obj);
}