console.log("IR Tool Running");

//global array holding the timings
var timings = [];

//global counter denoting sample number
var counter = 0;

//global variable holding current number of nodes in DOM tree
var numNodes;


/*In trying to obatin the malware extensions chrome storage we got to now that they live in isolated worlds.To show that this indeed 
is the case - ie seperate isolated storages, we added the retrieval mechanims to the content script of
the malware at the beginning and the same code in the on load event listener. As you can see in the docs, the malware displays its 
storage content, whereas the IR tool shows that it is empty*/

/*console.log(chrome.storage.sync.get(null, function(data){
    links = data;
    console.log(links);
}));*/

//Listens for requests by bg script, to capture evidence and send it to bg
//registerHTTPRequestListener();

//registering page startup events - to capture as soon as page starts downloading
//registerStartUpEvents();

//registering storage events - to capture any storage changes as soon as page starts downloading
//registerStorageEvents();

//capture relevant data and produce a json object containing it -- json format if param true else stringified json
function captureEvidence(event, json){
    //Trigger is the event that caused the evidence capture
    //Timestamp is the time at which evidece was collected
    //DOM is the DOM object collected
    //SessionStorage is the storage traces left during the session at points in time
    //LocalStorage is the storage traces left persisting in the page
    //chrome extension local storage of malware could not be captured at this level

    //Note that all methods are forced to return json object rather than stringified vers.
    //This is because if they take json parameter and it is set to false, they would be stringified
    //multiple times (exactly twice - one because of the captureEvidence and one because of the native
    //stringified clause). Like this they are only stringified once, depending on json bool value
    let evidence = {
        'Trigger': event,
        'TimeStamp': getTimeStamp(true),
        'DOMState': document.readyState, 
        'DOM': mapDOM(document.documentElement, true),
        'SessionStorage': window.sessionStorage,
        'LocalStorage': window.localStorage
    };

    return (json) ? evidence: JSON.stringify(evidence);
}

//sends evidence to background script -- which in turn makes request to server to save it
function sendEvidence(evidence){
    let msg = {
        'type': 'evidence',
        'evidence': evidence
    }
    chrome.runtime.sendMessage(msg);
}

//get current time details -- json format or stringified json
function getTimeStamp(json){
    let d = new Date();
    let days = ["Sunday","Monday","Teusday","Wendesday","Thursday","Friday","Saturday"];
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let time = {
        'Day': days[d.getDay()],
        'Month': months[d.getMonth()],
        'Date': d.getDate(),
        'Year': d.getFullYear(),
        'Time': d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() + ':' + d.getMilliseconds(),
        'TimeZone': 'UTC'+d.getTimezoneOffset()/60
    };
    return (json) ? time : JSON.stringify(time);
}



//original function can be found here : https://stackoverflow.com/questions/12980648/map-html-to-json

/*object placeholder/param is the current scope we are working in 
(current block of json - {})-- as soon as a new level is reached 
(i.e. child is found, add new scope to current one and start working in it through recursion). 
Scopes will be hierarchical with the highest one being the treeObject and as soon as we go down 
by one child add new scope. Because of recursion, as soon as we perfrom depth first traversal 
and we are closing off a current scope, sibling nodes will appear to be on the same level of 
scoping which is of desired behaviour. Also note that attributes style and values will be 
checked for as soon as current scope is being closed off, thus when recursion is tailing back up.*/

//element is the node we are starting our parsing from - to get whole document we use document.documentElement

//json boolean is a boolean which denotes whether we want our returned DOM parsed as json object or json string
function mapDOM(element, json) {
    var treeObject = {};

    //Recursively loop through DOM elements and assign properties to object
    function treeHTML(element, object) {
        object["type"] = element.nodeName;
        var nodeList = element.childNodes;
        if (nodeList != null) {
            if (nodeList.length) {
                object["content"] = [];
                for (var i = 0; i < nodeList.length; i++) {
                    if (nodeList[i].nodeType == 3) {
                        //length condition is introduced to eliminate extra enters and space
                        //nodes that are caught in content and are useless. however we cannot
                        //eliminate any node containing enter because injected scripts would //contain such characters
                        if(nodeList[i].nodeValue.length > 5){
                            object["content"].push(nodeList[i].nodeValue);
                        }                        
                    } else {
                        object["content"].push({});
                        treeHTML(nodeList[i], object["content"][object["content"].length -1]);
                    }
                }
            }
        }

        //as soon as we reached end of current scope, we check for style, attribute and element values
        if (element.attributes != null) {
            if (element.attributes.length) {
                object["attributes"] = {};
                for (var i = 0; i < element.attributes.length; i++) {
                    //if style is listed in attributes, ignore since it will be catered for
                    //in style object
                    if(element.attributes[i].nodeName !== "style"){
                        object["attributes"][element.attributes[i].nodeName] = element.attributes[i].nodeValue;
                    }
                }
            }
        }

        if (element.style != null && element.style.length > 0) {
            object["style"] = {};
            for (var i = 0; i < element.style.length; i++) {
                object["style"][element.style[i]] = element.style[element.style[i]];
            }
        }

        if (element.value != null){
            object["value"] = element.value;
        }
    }

    //call function
    treeHTML(element, treeObject);

    //if json param is set to true return json object else return string version of json object
    return (json) ? treeObject : JSON.stringify(treeObject) ;
}



/*Our json will be representing the following tree:

                                            HTML
                    Head                                           Body
    Title   Link     Link   Style   Style          H1   DIV DIV Script  Script  Script


    etc.*/

//function to capture events as soon as the page starts to download

//check out at which point the tampering with DOM starts
/*we are allowed to catch DOMContentLoaded because of the alteration in the manifest.json -- set to run_at document_start

https://developer.chrome.com/extensions/content_scripts#runtime

document_idle: browser uses time to inject contetn script between document_end and after window.onload event
document_end:   scripts are injected immediately after DOM is complete but before subresources like images have loaded
                (i.e. between readyState = interactive and readyState = complete (where complete is included, thus can be injected at load event))
document_start: scripts injected after css files are constructed and before DOM is contstructed or any other script is run

*/

/*DOMContentLoaded - the whole document (HTML) has been loaded.
load - the whole document and its resources (e.g. images, iframes, scripts) have been loaded.
The DOMContentLoaded event is fired when the document has been completely loaded and parsed, 
without waiting for stylesheets, images, and subframes to finish loading (the load event can be used to detect a fully-loaded page).*/

/*https://developer.mozilla.org/en-US/docs/Web/API/Document/readyState
DOMContentLoaded is trigerred as soon as document is inits interactive state, whilst load is trigerred when document is in complete state
interactive state: The document has finished loading and the document has been parsed but sub-resources such as images, stylesheets and frames are still loading.
complete state: The document and all sub-resources have finished loading. The state indicates that the load event is about to fire.
*/
function registerStartUpEvents(){
    //capture at init of page
    sendEvidence(captureEvidence({'init': window.document.readyState}, true));

    //capture as soon as dom is in interactive stage
    window.document.addEventListener('DOMContentLoaded', () => {
        sendEvidence(captureEvidence({'DOMContentLoaded': window.document.readyState}, true));

        //once that the DOM tree is loaded, start observing for mutations in DOM tree
        //where observers are based on specific nodes
        registerMutationObserver(document.documentElement, 'root');
    }); 

    //capture as soon as all resources have downloaded and page download is complete
    window.addEventListener('load', () => {
        sendEvidence(captureEvidence({"load": window.document.readyState},true));
        //5seconds after load, grab another snapshot
        setTimeout(function(){sendEvidence(captureEvidence({"post-load": window.document.readyState},true));},5000);

        //registering event listeners after page has loaded

        //registering mouse event listeners on buttons
        registerMouseEvents();

        //registering input event listeners on input fields
        registerInputEvents();
        
        //keep for demo purposes of isolated storages
        /*console.log(chrome.storage.sync.get(null, function(data){
            links = data;
            console.log('IR Tool Call');
            console.log(links);
        }));*/
    }); 
}

//function to add event listeners to all buttons on screen for different mouse events
function registerMouseEvents(){
    //get all buttons on the page
    let buttons = document.getElementsByClassName("btn btn-primary");

    //mouseenter and mouseover are similar however mouseover trigerred also when mouse enters or leaves
    //a child element of the bounded element besides the bounded element itself, whereas mouseenter
    //is trigerred only when mouse enters or leaves bounded element -- thus mouseenter is used

    //same concept can be said for mouseleave and mouseout, where event is trigerred when mouse is moved 
    //off the element that has the listener attached in the case of mouseleave, with similar behaviour in 
    //mouseout, however this time the behaviour is extended also to the nodes children -- thus mouseleave
    //is chosen

    //convert collection to array and for each button add the listeners
    Array.from(buttons).forEach(btn => {
        btn.addEventListener('click', () => sendEvidence(captureEvidence({'click': btn.id}, true)));
        btn.addEventListener('mouseenter', () => sendEvidence(captureEvidence({'mouseenter': btn.id}, true)));
        btn.addEventListener('mouseleave', () => sendEvidence(captureEvidence({'mouseleave': btn.id}, true)));
        btn.addEventListener('mousedown', () => sendEvidence(captureEvidence({'mousedown': btn.id}, true)));
        btn.addEventListener('mouseup', () => sendEvidence(captureEvidence({'mouseup': btn.id}, true)));
    });
}


//function to add event listeners to all input boxes on screen for different input events
function registerInputEvents(){
    //get all input elements on the page
    let inputFields = window.document.getElementsByTagName('input');

    //here we have two types of events either the field value was changed, else new input has been inputted.
    //in the latter case, this can either be captured by specific keyboard events - keydown, keyup,keyfind,
    //else by the generic oninput globaleventhandler. The global event handler was preferred over the specific
    //key event seems it was deemed not important to capture different key stroke behaviour. However overall, 
    //the change event was observed to make more sense to use over the oninput event. This is because the oninput
    //event would trigger with every key stroke the user submitted, making a lot of redundant captures, whereas
    //the change event is only trigerred once per total change in input field.

    //testing both cases it shows that neither the change event or oninput event are called when the values are
    //changed programmatically - as it would happen in a malware case, thus both events would not be useful to
    //trigger any valid evidence collection except for the original values that the user intended to input.

    //https://github.com/facebook/react/issues/8971 as described in here this is intended behaviour.

    Array.from(inputFields).forEach(field => {
        field.addEventListener('change', () => sendEvidence(captureEvidence({'change-input': field.id}, true)));
        /*field.oninput = function(){sendEvidence(captureEvidence({'on-input': field.id}, true))};*/
    });
}

//function to add event listeners to capture changes in local, session and chrome extension storage
function registerStorageEvents(){
    //since chrome extension storage of IR lives in seperate world from storage of malware, it would be useless
    //to try and capture changes in the storage, since you would only be capturing changes in the storage of your
    //own extension

    //we attempted to catch local storage and session storage changes however using this setup it did not work.
    //This is because the event listeners are part of the content script and thus living the environment the DApp
    //is launched in. Also the malware's content script is also living in such same environment. As per storage change
    //event documentation, this event is only triggered when the listener is listening on an instance/tab of the 
    //page, whereas the change in the localstorage of that page is caused in another instance/tab of that identical
    //page. Thus since both the potential identity causing such changes (i.e. malware) is living in the same tab
    //as the event listener is, such changes will not be caught, unless the user has two pages open of the same DApp.
    window.addEventListener('storage', event => {
        if(event.storageArea === sessionStorage){
            sendEvidence(captureEvidence({'storageChange': 'sessionStorage'}, true));
        }else{
            sendEvidence(captureEvidence({'storageChange': 'localStorage'}, true));
        }
    });
}

//registers mutations in the DOM tree

//ROOT NODE Observer : this could have been used nearly on it's own and eliminate all the other events, however it's downside 
//is that it causes a lot of false positives, since whenever the DOM is altered in the slightest of ways,
//eg. Vote count is updated automatically, it will capture the changes in the nodes in question, and capture
//evidence with each update, thus the evidence collected will increase exponentially.

//INPUT NODES Observer : this attempt was to try to detect any programmatic changes to the input fields. This is because as already
//mentioned before, this is not possible with the on changed event. However it was detected that this was not even possible using
//mutation observer. This is because according to the mutation observer documentation, it is able to detect changes in dom tree,
//wrt addition and removal of new nodes, changes in attributes and changes in character data (where these refere to any changes in
//text nodes). However when the input value is changed both programmatically as well as through js injection, this will result in
//a property change, rather than an attribute change, where the mutation observer will not be able to observe it; 
//(https://stackoverflow.com/questions/32383349/javascript-use-mutationobserver-to-detect-value-change-in-input-tag). Also given
//that an input element does not have a text node as a child node storing it's contents, even if the characterData property
//of the mutation observer is set to true, changes won't be detected. 

//Note on attributes vs properties: value of input field reflects current text-content inside input box, whereas value attribute 
//contains default/initial text-content of the value attribute that is set in the HTML source code (the one that shows up when 
//HTML page is loaded).https://stackoverflow.com/questions/6003819/what-is-the-difference-between-properties-and-attributes-in-html. 

function registerMutationObserver(node, evidenceNS){
    //https://developer.mozilla.org/en-US/docs/Web/API/MutationObserverInit
    //https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver/observe
    //https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver  

    let config = {attributes: true, childList: true, subtree:true, characterData:true};
    let observer = new MutationObserver(function(){
        sendEvidence(captureEvidence({'mutationDom': evidenceNS}, true));
    });

    //observe node selected and all of its children by default
    observer.observe(node, config);
}

//wehenever background script needs an evidence capture, which will only be trigerred from bg script because
//of intercepted HTTP requests, content script will send this to bg script. 
function registerHTTPRequestListener(){
    chrome.runtime.onMessage.addListener(msg => sendEvidence(captureEvidence({'HTTPRequest': msg.request_type},true)));
}

//function that times the capture and send evidence functions
function timeIt(){
    //count number of nodes
    numNodes = countNodes(document.documentElement)
    for (var i = 0; i<100;i++){
        var t0 = performance.now();
        var evidence = captureEvidence({'test': i}, true);
        var t1 = performance.now();
        let captureTime = t1-t0;
        t0 = performance.now();
        sendEvidence(evidence);
        t1 = performance.now();
        let sendingTime = t1-t0;
        let record = {
            'captureEvidence': captureTime,
            'sendBackground': sendingTime,
            'DOMSize': numNodes
        };
        timings.push({[counter] : record});
        counter+=1;
    }
}

//function that counts number of nodes
function countNodes(element) {
    let number_nodes = 1;
    //Recursively loop through DOM elements
    function treeHTML(element) {
        var nodeList = element.childNodes;
        if (nodeList != null) {
            if (nodeList.length) {
                for (var i = 0; i < nodeList.length; i++) {
                    number_nodes += 1;
                    treeHTML(nodeList[i]);
                }
            }
        }
    }

    //call function
    treeHTML(element);

    return number_nodes;
}

//function that increases the present number of nodes in the DOM Tree by 500 nodes
function enlargeTheDOM(){
    for(var i = 0 ; i < 200; i++){
        var p = document.createElement("p");
        document.body.appendChild(p);
    }
}

//function that drives the timing mechanisms
function timeDriver(){
    for(var i = 0; i<25; i++){
        timeIt();
        enlargeTheDOM();
    }
    let time = JSON.stringify(timings);
    //send timings to bg script for it to send to server
    let timingResult = {
        'type': 'timings',
        'timings': time
    }
    chrome.runtime.sendMessage(timingResult);
}

//start timing after everything has loaded -- wait 10seconds
setTimeout(timeDriver,10000);